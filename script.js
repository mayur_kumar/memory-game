const gameContainer = document.getElementById("game");

const COLORS = [
  "red",
  "blue",
  "green",
  "orange",
  "purple",
  "red",
  "blue",
  "green",
  "orange",
  "purple"
];

let clicks = []
let score = 0

function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}




function createDivsForColors(colorArray) {
  for (let color of colorArray) {

    const newDiv = document.createElement("div");
    newDiv.classList.add(color);
    newDiv.classList.add("white");
    newDiv.setAttribute("id",color)
    newDiv.addEventListener("click", handleCardClick);
    gameContainer.append(newDiv);

  }
}


function resetGame() {
  alert("you won the game")
  shuffledColors = shuffle(COLORS);
  score = 0 
  gameContainer.innerHTML  = ""
  createDivsForColors(shuffledColors);

}
// TODO: Implement this function!
function handleCardClick(event) {

  if(!this.classList.contains("white")) return 

  this.classList.remove("white");

  clicks.push(event.target)
  const [first,second] = [...clicks]
  
  if(clicks.length===2){
    let firstColor = first.getAttribute("id")
    let secondColor = second.getAttribute("id")
    if(firstColor === secondColor){

        score++
        if(score === 5){
          setTimeout(resetGame,500)
        }

    }else{

      setTimeout(()=>{
        first.classList.add("white")
        second.classList.add("white")
      },500)

    }
    clicks = []
  }

}


let shuffledColors = shuffle(COLORS);
createDivsForColors(shuffledColors);
